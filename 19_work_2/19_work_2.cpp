﻿#include <iostream>


class Animal
{
public:

    Animal()
    {}

    virtual void voice()
    {
        std::cout << "ANIMAL!!!" << std::endl;

    }
};


class Dog : public Animal
{
public:

    Dog()
    {}

    void voice() override
    {
        std::cout << "WOOF!!!" << std::endl;

    }

};



class Cat : public Animal
{
public:

    Cat()
    {}

    void voice() override
    {
        std::cout << "MEOW!!!" << std::endl;

    }

};



class Human : public Animal
{
public:

    Human()
    {}

    void voice() override
    {
        std::cout << "HI!!!" << std::endl;

    }

};







int main()
{   
    Animal** pA = new Animal * [3];
    Dog d;
    pA[0] = &d;
    Cat c;
    pA[1] = &c;
    Human h;
    pA[2] = &h;
    for (int i = 0; i < 3; i++)
    {
        pA[i]->voice();

    }


}